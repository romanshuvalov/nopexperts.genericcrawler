﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net.Repository.Hierarchy;
using NopExperts.Nop.Utils.GenericCrawler.Infrastructure;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace NopExperts.Nop.Utils.GenericCrawler
{
    class Program
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [STAThread]
        static void Main(string[] args)
        {
            Log.Info("Application started");
            ThreadPool.SetMaxThreads(20, 20);

            try
            {
                // get configuration
                Log.Info("Configuring...");

                if (args.Length < 1)
                {
                    throw new ArgumentException("Missing configuration file path");
                }

                string configurationFilePath = args[0];

                Log.Info("Loading configuration");

                var configuration = ConfigurationParser.GetConfiguration(configurationFilePath);

                Log.Info("Configuration loaded successfully");

                Log.Info("Processing...");
                var csParser = new CsWebPageParser();

                // load categories pages
                Log.Info("Step 1: download categories pages");
                var categoryLinks = configuration.Categories.Select(x => $"{configuration.BaseUrl}/{x.SubUrl}").ToList();
                var categoryFileNames = PageDownloader.DownloadPages(categoryLinks);

                // parse all links for pages
                var categoryPages = csParser.GetAllCategoriesPagingLinks(categoryFileNames, configuration);

                // add pages to all category links
                categoryFileNames.AddRange(PageDownloader.DownloadPages(categoryPages));

                // parse categories pages
                Log.Info("Step 2: parse all product links");
                var productLinks = csParser.GetAllProductLinks(categoryFileNames, configuration);

                // load products pages
                Log.Info("Step 3: download product pages");
                var productsFileNames = PageDownloader.DownloadPages(productLinks);

                Log.Info($"Products downloaded ({productsFileNames.Count})");
                Log.Info($"Saving to {configuration.OutputFile}");

                csParser.SaveProductsInfoToExcelFile(productsFileNames, configuration);

                SendMail(configuration.OutputFile);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            finally
            {
                Log.Info("End...");
                Console.ReadKey();
            }
        }

        static string ReadTextFromUrl(string url)
        {
            // WebClient is still convenient
            // Assume UTF8, but detect BOM - could also honor response charset I suppose
            using (var client = new WebClient())
            using (var stream = client.OpenRead(url))
            using (var textReader = new StreamReader(stream, Encoding.UTF8, true))
            {
                return textReader.ReadToEnd();
            }
        }
        
        static void SendMail(string filePath)
        {
            try
            {
                var mailSubject = ConfigurationManager.AppSettings["MailSubject"];
                var mailToListString = ConfigurationManager.AppSettings["ToMailList"];

                Log.Info($"Sending email to {mailToListString}...");

                var mailToList = mailToListString.Split(',');

                var mail = new MailMessage();

                foreach (var mailAddress in mailToList)
                {
                    mail.To.Add(mailAddress);
                }

                mail.Subject = mailSubject;
                mail.SubjectEncoding = Encoding.UTF8;
                mail.BodyEncoding = Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                mail.Attachments.Add(new Attachment(filePath));

                var client = new SmtpClient();

                client.Send(mail);
                Log.Info("Email sent!");
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
    }
}
