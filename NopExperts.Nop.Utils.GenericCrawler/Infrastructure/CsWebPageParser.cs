﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace NopExperts.Nop.Utils.GenericCrawler.Infrastructure
{
    public class CsWebPageParser
    {
        /// <summary>
        /// Loads all founded on categories pages products to files
        /// </summary>
        /// <param name="fileNames">list of file names</param>
        /// <param name="configuration">configuration object</param>
        /// <returns>
        /// list of links
        /// </returns>
        public List<string> GetAllProductLinks(IEnumerable<string> fileNames, CrawlConfig configuration)
        {
            var result = new List<string>();

            //Utils.ConsoleProgress("Products count: 0", true);

            Task.WaitAll(fileNames.Select(x =>
            {
                return Task.Factory.StartNew(() => ParseProductLinksFromFile(x, configuration))
                    .ContinueWith((task) =>
                    {
                        lock (result)
                        {
                            result.AddRange(task.Result);
                        }
                    });
            }).ToArray());

            Console.WriteLine($"ProductCount: {result.Count}");
            Console.WriteLine();

            return result;
        }

        public List<string> ParseProductLinksFromFile(string fileName, CrawlConfig configuration)
        {
            var dom = CsQuery.CQ.CreateFromFile($"TempPages/{fileName}");

            var productLinks = dom[configuration.Selectors.ProductLink]
                .Elements
                .Select(x => x.Attributes.FirstOrDefault(CheckAttribute).Value)
                .Where(x => x != null)
                .ToList();

            return productLinks;
        }

        public List<string> GetAllCategoriesPagingLinks(IEnumerable<string> fileNames, CrawlConfig configuration)
        {
            var result = new List<string>();

            Task.WaitAll(fileNames.Select(x =>
            {
                return Task.Factory.StartNew(() => ParseCategoryPagesLinks(x, configuration))
                    .ContinueWith((task) =>
                    {
                        lock (result)
                        {
                            result.AddRange(task.Result);
                        }
                    });
            }).ToArray());
            
            return result;
        }

        public List<string> ParseCategoryPagesLinks(string fileName, CrawlConfig configuration)
        {
            var dom = CsQuery.CQ.CreateFromFile($"TempPages/{fileName}");

            var pagesLinks = dom[configuration.Selectors.PagingLink]
              .Elements
              .Select(x => x.Attributes.FirstOrDefault(CheckAttribute).Value)
              .Where(x => x != null)
              .ToList();

            return pagesLinks;
        }

        private bool CheckAttribute(KeyValuePair<string, string> attribute)
        {
            return attribute.Key == "href"
                   && !string.IsNullOrEmpty(attribute.Value)
                   && attribute.Value != "#";
        }

        public void SaveProductsInfoToExcelFile(List<string> productFiles, CrawlConfig configuration)
        {
            var stream = new FileStream(configuration.OutputFile, FileMode.Create);

            using (var xlPackage = new ExcelPackage(stream))
            {
                int iRow = 2;
                var worksheet = xlPackage.Workbook.Worksheets.Add("Crawled products");

                // creating column titles
                var properties = configuration.Properties.Select(x => x.DisplayName).ToArray();

                for (int i = 0; i < properties.Length; i++)
                {
                    worksheet.Cells[1, i + 1].Value = properties[i];
                    worksheet.Cells[1, i + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[1, i + 1].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
                    worksheet.Cells[1, i + 1].Style.Font.Bold = true;
                }

                // writing lines
                Task.WaitAll(productFiles.Select(x =>
                 {
                     return Task.Factory
                     .StartNew(() => ParseProductProperties(x, configuration))
                     .ContinueWith(task =>
                     {
                         // when product is parsed
                         var disctionary = task.Result;
                         lock (worksheet)
                         {
                             int i = 1;
                             foreach (var keyValue in disctionary)
                             {
                                 worksheet.Cells[iRow, i].Value = keyValue.Value;
                                 i++;
                             }

                             iRow++;
                         }
                     });
                 }).ToArray());

                xlPackage.Save();
            }
        }

        public Dictionary<string, string> ParseProductProperties(string productFileName, CrawlConfig configuration)
        {
            var result = new Dictionary<string, string>();
            var dom = CsQuery.CQ.CreateFromFile($"TempPages/{productFileName}");

            foreach (var propertyConfig in configuration.Properties)
            {
                var value = dom[propertyConfig.Selector].Text();

                if (string.IsNullOrEmpty(value))
                {
                    value = propertyConfig.DefaultValue;
                }

                result.Add(propertyConfig.Name, value);
            }

            return result;
        }

    }
}