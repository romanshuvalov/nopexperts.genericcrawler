﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace NopExperts.Nop.Utils.GenericCrawler.Infrastructure
{
    public class CrawlConfig
    {
        [JsonProperty("baseUrl")]
        public string BaseUrl { get; set; }

        [JsonProperty("outputFile")]
        public string OutputFile { get; set; }

        [JsonProperty("categoires")]
        public List<CategoriesConfig> Categories { get; set; }

        [JsonProperty("selectors")]
        public SelectorsConfig Selectors { get; set; }

        [JsonProperty("properties")]
        public List<PropertyConfig> Properties { get; set; }
    }

    public class CategoriesConfig
    {
        [JsonProperty("subUrl")]
        public string SubUrl { get; set; }
    }

    public class SelectorsConfig
    {
        [JsonProperty("productLink")]
        public string ProductLink { get; set; }
        [JsonProperty("pagingLink")]
        public string PagingLink { get; set; }
    }

    public class PropertyConfig
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("selector")]
        public string Selector { get; set; }

        [JsonProperty("defaultValue")]
        public string DefaultValue { get; set; }
    }
}