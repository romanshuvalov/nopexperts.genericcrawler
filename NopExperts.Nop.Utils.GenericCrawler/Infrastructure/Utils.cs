﻿using System;

namespace NopExperts.Nop.Utils.GenericCrawler.Infrastructure
{
    public static class Utils
    {
        public static void ConsoleProgress(string str, bool isStart = false)
        {
            if (isStart)
                Console.Write(str);
            else
                Console.Write("\r{0}", str);
        }
    }
}