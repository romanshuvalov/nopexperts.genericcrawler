﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using mshtml;
using SHDocVw;

namespace NopExperts.Nop.Utils.GenericCrawler.Infrastructure
{
    public class PageDownloader : IDisposable
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private InternetExplorer _webBrowser;
        private readonly ManualResetEventSlim _mres;
        private string _fileName = String.Empty;
        private string _downloadingUrl = string.Empty;

        public PageDownloader()
        {
            _mres = new ManualResetEventSlim(false);
        }

        /// <summary>
        /// Download list of pages to files
        /// </summary>
        /// <param name="urlsList">Page URLS</param>
        /// <returns>List of file names</returns>
        public static List<string> DownloadPages(IEnumerable<string> urlsList)
        {
            List<string> fileNames = new List<string>();

            Task.WaitAll(urlsList.Select(x =>
            {
                using (var pageDonwloader = new PageDownloader())
                {
                    string fileName = x.Split('/').Last();

                    if (!string.IsNullOrEmpty(fileName))
                    {
                        fileName = Regex.Replace(fileName, @"\.|\?", "_");
                        fileName += ".html";
                    }

                    lock (fileNames)
                    {
                        fileNames.Add(fileName);
                    }

                    return pageDonwloader.DownloadPage(x, fileName);
                }
            }).ToArray());

            return fileNames;
        }

        public Task DownloadPage(string pageUrl, string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentException("fileName must not be empty", nameof(fileName));
            }

            if (string.IsNullOrEmpty(pageUrl))
            {
                throw new ArgumentException("pageUrl must not be empty", nameof(pageUrl));
            }

            _fileName = fileName;
            _downloadingUrl = pageUrl;

            return Task.Factory.StartNew(() =>
            {
                try
                {
                    _webBrowser = new InternetExplorer();

                    object mVal = System.Reflection.Missing.Value;
                    _webBrowser.Navigate(pageUrl, ref mVal, ref mVal, ref mVal, ref mVal);

                    _webBrowser.DocumentComplete += document_loaded;

                    _webBrowser.Visible = false;

                    _mres.Wait();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    _webBrowser?.Quit();
                    _mres.Set();
                }
            });
        }

        private void document_loaded(object pDisp, ref object url)
        {
            try
            {
                // to prevent double shooting
                _webBrowser.DocumentComplete -= document_loaded;

                IHTMLDocument2 htmlDoc = (IHTMLDocument2)_webBrowser.Document;

                var str = htmlDoc.body;

                if (!Directory.Exists("TempPages"))
                {
                    Directory.CreateDirectory("TempPages");
                }

                File.WriteAllText($"TempPages/{_fileName}", str.innerHTML);

                Log.Info($"Page \"{_downloadingUrl}\" donwloded to {_fileName}");
            }
            finally
            {
                _webBrowser.Quit();
                _mres.Set();

                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(_webBrowser);
            }
        }

        public void Dispose()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}