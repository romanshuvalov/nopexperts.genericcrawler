﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace NopExperts.Nop.Utils.GenericCrawler.Infrastructure
{
    public static class ConfigurationParser
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static CrawlConfig GetConfiguration(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentException("Missing configuration file path", nameof(filePath));
            }

            if (!File.Exists(filePath))
            {
                throw new ArgumentException($"Missing configuration file ({filePath})");
            }

            var jsonString = File.ReadAllText(filePath);

            var configuration = JsonConvert.DeserializeObject<CrawlConfig>(jsonString);

            if (configuration == null)
            {
                throw new FormatException("Wrong configuration file format");
            }

            Log.Debug(JsonConvert.SerializeObject(configuration));

            return configuration;
        }
    }
}